package com.springbootproject.springproject.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import sun.misc.BASE64Encoder;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by student on 2/23/2018.
 */

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "person_type")
@Table(schema = "public", name = "persons")
public abstract class Person {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    @Column(name = "date_of_birth")
    private java.util.Date dateOfBirth;

    private String gender;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

    @OneToOne(optional=false)
    @JoinColumn(name = "library_abonament_id")
    private LibraryAbonament libraryAbonament;
    @Column(name = "picture")
    private byte[] img;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "persons_to_phones",
            joinColumns = { @JoinColumn(name = "person_id") },
            inverseJoinColumns = { @JoinColumn(name = "phone_id") }
    )
    private List<Phone> phones;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public LibraryAbonament getLibraryAbonament() {
        return libraryAbonament;
    }

    public void setLibraryAbonament(LibraryAbonament libraryAbonament) {
        this.libraryAbonament = libraryAbonament;
    }

    public byte[] getImg() {
        return img;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }
}
