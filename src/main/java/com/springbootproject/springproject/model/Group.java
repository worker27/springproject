package com.springbootproject.springproject.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by student on 2/22/2018.
 */

@Entity
@Table(schema = "public", name = "groups")
public class Group {
    @Id
    private Long id;
    private String name;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
