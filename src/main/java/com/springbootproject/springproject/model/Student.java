package com.springbootproject.springproject.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by student on 2/23/2018.
 */
@Entity(name = "Student")
@DiscriminatorValue(value = "s")
@Table(schema = "public", name = "students")
public class Student extends Person {



    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="group_id", referencedColumnName = "id")
    private Group group;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "disciplines_to_students",
            joinColumns = { @JoinColumn(name = "student_id") },
            inverseJoinColumns = { @JoinColumn(name = "discipline_id") }
    )
    private List<Discipline> disciplines;

    @OneToMany(mappedBy="student")
    private List<Mark> marks;

    public Group getGroup() {
        return group;
    }

    public List<Discipline> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(List<Discipline> disciplines) {
        this.disciplines = disciplines;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<Mark> getMarks() {
        return marks;
    }

    public void setMarks(List<Mark> marks) {
        this.marks = marks;
    }
}
