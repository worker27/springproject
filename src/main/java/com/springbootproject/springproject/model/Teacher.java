package com.springbootproject.springproject.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by student on 2/26/2018.
 */
@Entity
@DiscriminatorValue(value = "t")
@Table(schema = "public", name = "teachers")
public class Teacher extends Person {

    private Integer salary;

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }
}
