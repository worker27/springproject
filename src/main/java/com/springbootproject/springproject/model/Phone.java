package com.springbootproject.springproject.model;

import javax.persistence.*;

/**
 * Created by student on 2/22/2018.
 */
@Entity
@Table(schema = "public", name = "phones")
public class Phone {
    @Id
    private Long id;
    private String value;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type_id", referencedColumnName = "id")
    private PhoneType phoneType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public PhoneType getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(PhoneType phoneType) {
        this.phoneType = phoneType;
    }
}
