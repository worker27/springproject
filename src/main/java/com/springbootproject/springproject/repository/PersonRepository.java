package com.springbootproject.springproject.repository;

import com.springbootproject.springproject.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 * Created by student on 2/23/2018.
 */
@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {
}
