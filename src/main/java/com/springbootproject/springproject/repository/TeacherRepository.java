package com.springbootproject.springproject.repository;

import com.springbootproject.springproject.model.Teacher;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by student on 2/26/2018.
 */
@Repository
public interface TeacherRepository extends CrudRepository<Teacher, Long> {
}
