package com.springbootproject.springproject.repository;

import com.springbootproject.springproject.model.PhoneType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by student on 2/21/2018.
 */
@Repository
public interface PhoneTypeRepository extends CrudRepository<PhoneType, Long>{
}
