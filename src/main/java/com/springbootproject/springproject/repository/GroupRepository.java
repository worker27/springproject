package com.springbootproject.springproject.repository;

import com.springbootproject.springproject.model.Group;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by student on 2/22/2018.
 */
@Repository
public interface GroupRepository extends CrudRepository<Group, Long>{
    List<Group> findAll();
   // Group findById();
}
