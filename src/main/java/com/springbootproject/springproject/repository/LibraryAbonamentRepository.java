package com.springbootproject.springproject.repository;

import com.springbootproject.springproject.model.LibraryAbonament;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by student on 2/23/2018.
 */
@Repository
public interface LibraryAbonamentRepository extends CrudRepository<LibraryAbonament, Long>{
}
