package com.springbootproject.springproject.repository;

import com.springbootproject.springproject.model.Mark;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by student on 2/26/2018.
 */
@Repository
public interface MarkRepository extends CrudRepository<Mark, Long> {
}
