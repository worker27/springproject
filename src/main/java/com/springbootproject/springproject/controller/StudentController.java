package com.springbootproject.springproject.controller;

import com.springbootproject.springproject.model.Student;
import com.springbootproject.springproject.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by student on 2/23/2018.
 */
@RestController
@RequestMapping("/students")
@CrossOrigin(origins = "http://localhost:4200")
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;

    @GetMapping("/findAll")
    public List<Student> getStudents(){
        return (List<Student>) studentRepository.findAll();
    }

    @PostMapping("/add")
    public ResponseEntity<Student> add(@RequestBody Student student){
        studentRepository.save(student);
        return ResponseEntity.ok(student);
    }
}
