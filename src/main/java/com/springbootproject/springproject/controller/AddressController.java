package com.springbootproject.springproject.controller;

import com.springbootproject.springproject.model.Address;
import com.springbootproject.springproject.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by student on 2/22/2018.
 */
@RestController
@RequestMapping("/addresses")
@CrossOrigin(origins = "http://localhost:4200")
public class AddressController {

    @Autowired
    private AddressRepository addressRepository;

    @GetMapping("/findAll")
    public List<Address> getAddresses(){
        return (List<Address>) addressRepository.findAll();
    }
}
