package com.springbootproject.springproject.controller;

import com.springbootproject.springproject.model.Phone;
import com.springbootproject.springproject.repository.PhoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by student on 2/22/2018.
 */

@RestController
@RequestMapping("/phones")
@CrossOrigin(origins = "http://localhost:4200")
public class PhoneController {

    @Autowired
    private PhoneRepository phoneRepository;

    @GetMapping("/findAll")
    public List<Phone> getPhones(){
        return (List<Phone>) phoneRepository.findAll();
    }
}
