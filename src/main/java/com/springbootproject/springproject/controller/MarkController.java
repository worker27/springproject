package com.springbootproject.springproject.controller;

import com.springbootproject.springproject.model.Mark;
import com.springbootproject.springproject.repository.MarkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by student on 2/26/2018.
 */
@RestController
@RequestMapping("/marks")
@CrossOrigin(origins = "http://localhost:4200")
public class MarkController {
    @Autowired
    private MarkRepository markRepository;

    @GetMapping("/findAll")
    public List<Mark> getMarks(){
        return (List<Mark>) markRepository.findAll();
    }
}
