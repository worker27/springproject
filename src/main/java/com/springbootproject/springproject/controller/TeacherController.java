package com.springbootproject.springproject.controller;

import com.springbootproject.springproject.model.Teacher;
import com.springbootproject.springproject.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by student on 2/26/2018.
 */
@RestController
@RequestMapping("/teachers")
@CrossOrigin(origins = "http://localhost:4200")
public class TeacherController {
    @Autowired
    private TeacherRepository teacherRepository;

    @GetMapping("/findAll")
    public List<Teacher> getTeachers(){
        return (List<Teacher>) teacherRepository.findAll();
    }
}
