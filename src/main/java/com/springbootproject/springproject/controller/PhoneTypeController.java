package com.springbootproject.springproject.controller;

import com.springbootproject.springproject.model.PhoneType;
import com.springbootproject.springproject.repository.PhoneTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/21/2018.
 */

@RestController
@RequestMapping("/phoneTypes")
@CrossOrigin(origins = "http://localhost:4200")
public class PhoneTypeController {

    @Autowired
    private PhoneTypeRepository phoneTypeRepository;

    @GetMapping("/findAll")
    public List<PhoneType> getPhoneTypes() {
        return (List<PhoneType>) phoneTypeRepository.findAll();
    }
}

