package com.springbootproject.springproject.controller;

import com.springbootproject.springproject.model.Person;
import com.springbootproject.springproject.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by student on 2/23/2018.
 */
@RestController
@RequestMapping("/people")
@CrossOrigin(origins = "http://localhost:4200")
public class PersonController {
    @Autowired
    private PersonRepository personRepository;

    @GetMapping("/findAll")
    public List<Person> getPeople(){
        return (List<Person>) personRepository.findAll();
    }
}
