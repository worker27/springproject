package com.springbootproject.springproject.controller;

import com.springbootproject.springproject.model.Group;
import com.springbootproject.springproject.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by student on 2/22/2018.
 */

@RestController
@RequestMapping("/groups")
@CrossOrigin(origins = "http://localhost:4200")
public class GroupController {

    @Autowired
    private GroupRepository groupRepository;

    @GetMapping("/findAll")
    public List<Group> getGroups(){
        return groupRepository.findAll();
    }
}
