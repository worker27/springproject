package com.springbootproject.springproject.controller;

import com.springbootproject.springproject.model.LibraryAbonament;
import com.springbootproject.springproject.repository.LibraryAbonamentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by student on 2/23/2018.
 */
@RestController
@RequestMapping("/libraryAbonaments")
@CrossOrigin(origins = "http://localhost:4200")
public class LibraryAbonamentController {
    @Autowired
    private LibraryAbonamentRepository libraryAbonamentRepository;

    @GetMapping("/findAll")
    public List<LibraryAbonament> getLibraryAbonaments(){
        return (List<LibraryAbonament>) libraryAbonamentRepository.findAll();
    }
}
